// Defining class
class Info {
  var appName;
  var appCategory;
  var appDeveloper;
  var appYear;

  // function to convert letters to uppercase
  displayApp() {
    print("The app name to Upper case is : ${appName.toUpperCase()}");
  }

  // defining class function
  showAppInfo() {
    print("App of the year is : $appName");
    print("sector/category is : $appCategory");
    print("Developer is : $appDeveloper");
    print("Year is : $appYear");
  }
}

//main function
void main() {
  //main function begins

  // Creating object called app
  var app = new Info();
  app.appName = "FNB Banking ";
  app.appCategory = "best Android consumer App";
  app.appDeveloper = "MTN Team";
  app.appYear = "2012";

// Accessing class Functions
  app.showAppInfo();

  app.displayApp();
}
//main function end


